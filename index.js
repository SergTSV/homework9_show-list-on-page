const testWrapper = document.querySelector(".test-wrapper");

const data1 = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const data2 = ["1", "2", "3", "sea", "user", 23];
const data3 = [
  "Kharkiv",
  "Kiev",
  ["Borispol", "Irpin", ["1", "2", "3", "sea", "user", 23]],
  "Odessa",
  "Lviv",
  "Dnieper",
];

const createNewElement = function (array, parent = document.body) {
  const list = document.createElement("ul");
  list.className = "list";
  parent.append(list);

  array.forEach((value) => {
    if (!Array.isArray(value)) {
      const newElement = document.createElement(`li`);
      newElement.textContent = value;
      list.append(newElement);
    } else {
      list.append(createNewElement(value));
    }
  });

  return list;
};

const timer = (second = 3) => {
  const pTimer = document.createElement("p");
  document.body.append(pTimer);
  pTimer.style.textAlign = "center";
  pTimer.textContent = `The window will clear through ${second} sec.`;
  setInterval(function () {
    if (second > 1) {
      second--;
      pTimer.textContent = `The window will clear through ${second} sec.`;
    } else {
      clearInterval(timer);
      document.body.hidden = true;
    }
  }, 800);
};

createNewElement(data1, testWrapper);
createNewElement(data2, testWrapper);
createNewElement(data3, testWrapper);
timer();
